<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = 'shop_products';

	protected $fillable = ['category_id', 'slug', 'name'];

	public function category()
	{
		return $this->belongsToMany('\Modules\Shop\Entities\Category', 'shop_category_product');
	}

	public function url()
	{
		return $this->morphOne('\Modules\Shop\Entities\Url', 'model');
	}
}