<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
	protected $table = 'shop_url';

	public $timestamps = false;

	protected $fillable = ['path'];

	public function model()
	{
		return $this->morphTo();
	}
}
