<?php

namespace Modules\Shop\Entities;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
	use NodeTrait;

	protected $table = 'shop_category';

	protected $fillable = ['parent_id', 'slug', 'name'];

	public function parent()
	{
		return $this->belongsTo(self::class);
	}

	public function url()
	{
		return $this->morphOne('\Modules\Shop\Entities\Url', 'model');
	}

	public function products()
	{
		return $this->belongsToMany('\Modules\Shop\Entities\Product', 'shop_category_product');
	}

	public function generatePath($slug)
	{
		if ($this->parent_id === 0) {
			$this->path = $slug;
		} else {
			$this->path = $this->url->path . '/' . $slug;
		}

		return $this;
	}
}
