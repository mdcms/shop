<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface UrlRepository extends BaseRepository
{
	public function find($id);
}