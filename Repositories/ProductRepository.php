<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface ProductRepository extends BaseRepository
{
	public function find($id);

	public function getProducts($category);

	public function getSubProducts($category);

	public function getProductsByIds($ids);

	public function loadUrlForProducts($products);
}