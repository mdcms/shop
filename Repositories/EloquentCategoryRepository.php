<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\EloquentBaseRepository;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Url;

class EloquentCategoryRepository extends EloquentBaseRepository implements CategoryRepository
{
	public function getModelByPath($path)
	{
		if (!$path) return Category::find(1);
		else {
			$url = new Url;
			$model = $url->wherePath($path)->first();
			return $model->model;
		}
	}

	public function getSubCategories($category)
	{
		$categories = $category->whereParentId($category->id)->whereStatus(1)->orderBy('_lft')->get();
		$categories->load('url');
		return $categories;
	}
}