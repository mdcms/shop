<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\EloquentBaseRepository;

class EloquentUrlRepository extends EloquentBaseRepository implements UrlRepository
{
	public function find($id)
	{
		return $this->model->find($id);
	}
}