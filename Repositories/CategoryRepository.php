<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface CategoryRepository extends BaseRepository
{
	public function getModelByPath($path);

	public function getSubCategories($category);
}