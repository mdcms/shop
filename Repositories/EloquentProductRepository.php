<?php
namespace Modules\Shop\Repositories;

use Modules\Core\Repositories\EloquentBaseRepository;
use Modules\Shop\Entities\Product;

class EloquentProductRepository extends EloquentBaseRepository implements ProductRepository
{
	public function find($id)
	{
		return $this->model->find($id);
	}

	public function getProducts($category)
	{
		return $category->products()->whereStatus(1)->paginate(config('shop.catalog.paginate'));
	}

	public function getSubProducts($category)
	{
		$descendants = $category->descendants()->get();

		if ($descendants->count() > 0) {
			$c_ids = [];

			if ($descendants->count() > 0) {
				foreach ($descendants as $descendant) {
					$c_ids[] = $descendant->id;
				}
			}

			$products = $this->getProductsByIds($c_ids);
		} else
			$products = $this->getProducts($category);

		$this->loadUrlForProducts($products);

		return $products;
	}

	public function getProductsByIds($ids)
	{
		$product = new Product;
		return $product->join('shop_category_product', 'shop_category_product.product_id', '=', 'shop_products.id')
			->whereIn('shop_category_product.category_id', $ids)
			->whereStatus(1)
			->paginate(config('shop.catalog.paginate'));
	}

	public function loadUrlForProducts($products)
	{
		if (count($products->items()) > 0) $products->load('url');
	}
}