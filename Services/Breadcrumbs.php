<?php
namespace Modules\Shop\Services;

class Breadcrumbs
{
	public static function get($category, $product = null)
	{
		$breadcrumbs = [];

		$ancestors = $category->ancestors()->get();
		$ancestors = $ancestors->push($category);

		if ($ancestors->count() > 0) {
			$breadcrumbs[] = [
				'name' => $ancestors[0]->name,
				'url' => $ancestors[0]->slug,
			];

			$slug = '';

			foreach ($ancestors as $ancestor) {
				if ($ancestor->id != 1) {
					$slug = $slug . '/' . $ancestor->slug;
					$breadcrumbs[] = [
						'name' => $ancestor->name,
						'url' => $breadcrumbs[0]['url'] . $slug,
					];
				}
			}

			if ($product) {
				$path = end($breadcrumbs);
				$breadcrumbs[] = [
					'name' => $product->name,
					'url' => $path['url'] . '/' . $product->slug,
				];
			}
		}

		return collect(json_decode(json_encode($breadcrumbs)));
	}
}