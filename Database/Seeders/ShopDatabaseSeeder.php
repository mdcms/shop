<?php

namespace Modules\Shop\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Shop\Entities\Category;

class ShopDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		\DB::table('shop_category')->truncate();
		Category::create(
			[
				'parent_id' => 0,
				'_lft' => 1,
				'_rgt' => 2,
				'slug' => 'catalog',
				'name' => 'Каталог',
				'status' => 1,
				'show_sub_category' => 1,
				'show_products' => 1,
				'show_sub_products' => 1,
			]
		);
		Model::reguard();
	}
}
