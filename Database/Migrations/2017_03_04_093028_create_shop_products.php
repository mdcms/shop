<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopProducts extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shop_products', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('category_id', false)->default(0);
			$table->string('slug')->nullable();
			$table->string('name')->nullable();
			$table->string('title')->nullable();
			$table->string('key')->nullable();
			$table->string('desc')->nullable();
			$table->longText('body')->nullable();
			$table->boolean('status')->default(false);
			$table->timestamps();
			$table->unique(['category_id', 'slug']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('shop_products');
	}
}
