<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopCategory extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('shop_category', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('parent_id', false)->default(0);
			$table->unsignedInteger('_lft');
			$table->unsignedInteger('_rgt');
			$table->string('slug')->nullable();
			$table->string('name')->nullable();
			$table->string('title')->nullable();
			$table->string('key')->nullable();
			$table->string('desc')->nullable();
			$table->longText('body')->nullable();
			$table->string('img')->nullable();
			$table->boolean('status')->default(false);
			$table->boolean('show_sub_category')->default(false);
			$table->boolean('show_products')->default(false);
			$table->boolean('show_sub_products')->default(false);
			$table->timestamps();
			$table->unique(['parent_id', 'slug']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('shop_category');
	}
}
