<?php

namespace Modules\Shop\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Modules\Shop\Entities\Category;
use Modules\Shop\Entities\Product;
use Modules\Shop\Entities\Url;
use Modules\Shop\Repositories\EloquentCategoryRepository;
use Modules\Shop\Repositories\EloquentProductRepository;

class ShopServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerBindings();

		Relation::morphMap([
			'category' => \Modules\Shop\Entities\Category::class,
			'product' => \Modules\Shop\Entities\Product::class,
		]);
	}

	/**
	 * Register config.
	 *
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
			__DIR__ . '/../Config/config.php' => config_path('shop.php'),
		], 'config');
		$this->mergeConfigFrom(
			__DIR__ . '/../Config/config.php', 'shop'
		);
	}

	/**
	 * Register views.
	 *
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = base_path('resources/views/modules/shop');

		$sourcePath = __DIR__ . '/../Resources/views';

		$this->publishes([
			$sourcePath => $viewPath
		]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/shop';
		}, \Config::get('view.paths')), [$sourcePath]), 'shop');
	}

	/**
	 * Register translations.
	 *
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = base_path('resources/lang/modules/shop');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'shop');
		} else {
			$this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'shop');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

	private function registerBindings()
	{
		$this->app->bind(
			'Modules\Shop\Repositories\CategoryRepository',
			function () {
				return new EloquentCategoryRepository(new Category());
			}
		);
		$this->app->bind(
			'Modules\Shop\Repositories\ProductRepository',
			function () {
				return new EloquentProductRepository(new Product());
			}
		);
		$this->app->bind(
			'Modules\Shop\Repositories\UrlRepository',
			function () {
				return new EloquentUrlRepository(new Url());
			}
		);
	}
}
