<?php
Route::group(['middleware' => ['api', 'permissions'], 'prefix' => config('core.api_url') . '/catalog', 'namespace' => 'Modules\Shop\Http\Controllers', 'as' => 'catalog::'], function () {
	Route::resource('category', 'ApiCategoryController', [
		'only' => [
			'index',
			'store',
			'edit',
			'update',
			'destroy',
		]
	]);
	Route::resource('product', 'ApiProductController', [
		'only' => [
			'index',
			'store',
			'edit',
			'update',
			'destroy',
		]
	]);
});

Route::group(['middleware' => ['web', 'theme'], 'prefix' => 'catalog', 'namespace' => 'Modules\Shop\Http\Controllers'], function () {
	Route::get('{path?}', 'PublicShopController@category_product')->where('path', '[a-zA-Z0-9/_-]+');;
});
