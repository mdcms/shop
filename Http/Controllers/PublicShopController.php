<?php

namespace Modules\Shop\Http\Controllers;

use Illuminate\Routing\Controller;
use Modules\Shop\Entities\Category;
use Modules\Shop\Repositories\CategoryRepository;
use Modules\Shop\Repositories\ProductRepository;
use Modules\Shop\Services\Breadcrumbs;

class PublicShopController extends Controller
{
	private $Category;
	private $Product;

	public function __construct(CategoryRepository $Category, ProductRepository $Product)
	{
		$this->Category = $Category;
		$this->Product = $Product;
	}

	public function category_product($path = null)
	{
		$categories = null;
		$products = null;
		$product = null;

		$category = $this->Category->getModelByPath($path);

		if ($category instanceof Category) {

			$breadcrumbs = Breadcrumbs::get($category);

			if ($category->show_sub_category == 1) $categories = $this->Category->getSubCategories($category);

			if ($category->show_products == 1) {
				if ($category->show_sub_products == 1) {
					$products = $this->Product->getSubProducts($category);
				} else
					$products = $this->Product->getProducts($category);
			}

			return \Theme::view('modules.shop.category',
				[
					'breadcrumbs' => $breadcrumbs,
					'category' => $category,
					'categories' => $categories,
					'products' => $products,
				]
			);
		} else {
			$product = $category;

			$category = $this->Category->find($product->category_id);

			$breadcrumbs = Breadcrumbs::get($category, $product);

			return \Theme::view('modules.shop.product',
				[
					'breadcrumbs' => $breadcrumbs,
					'category' => $category,
					'product' => $product,
				]
			);
		}
	}
}
