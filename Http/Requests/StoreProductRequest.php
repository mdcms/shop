<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$post = $this->request->all();

		if (!isset($post['category_id'])) $post['category_id'] = 0;
		if (!isset($post['category_ids'])) $post['category_ids'][0] = $post['category_id'];

		return [
			'category_id' => 'integer|exists:shop_category,id,id,' . $post['category_id'],
			'name' => 'required|min:3',
			'slug' => 'required|min:1|max:255|unique:shop_products,slug,NULL,id,category_id,' . $post['category_ids'][0],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
