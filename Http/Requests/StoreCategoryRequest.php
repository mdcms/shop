<?php

namespace Modules\Shop\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$post = $this->request->all();

		if (!isset($post['parent_id'])) $post['parent_id'] = 1;

		return [
			'parent_id' => 'integer|exists:shop_category,id,id,' . $post['parent_id'],
			'name' => 'required|min:3',
			'slug' => 'required|min:1|max:255|unique:shop_category,slug,NULL,id,parent_id,' . $post['parent_id'],
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
}
